var myApp = angular.module('myApp', ['ng-admin', 'ui.ace']);


//var customCreate = customCreateTemplate;
myApp.config(['NgAdminConfigurationProvider', function (nga) {
        var admin = nga.application('Admin dinamico').baseApiUrl('/cfx-rest-services/api/');




        /* interfaces */
        var interface = nga.entity('interface');
        interface.listView().fields([
            nga.field('path'),
            nga.field('interface')
        ]);

        /* servers */
        var server = nga.entity('server');
        server.listView().fields([
            nga.field('id'),
            nga.field('serverName'),
            nga.field('status'),
            nga.field('port')
        ]).listActions([
            '<start-server entry="entry"></start-server>',
            '<stop-server entry="entry"></stop-server>'
        ]);
        server.creationView().fields([
            nga.field('serverName'),
            nga.field('port')
        ]);
        server.editionView().fields(server.creationView().fields());

        /* scripts */
        var aceJavaOption = "{mode: 'java', require: ['ace/ext/language_tools'],advanced: {enableSnippets: true, enableBasicAutocompletion: true, enableLiveAutocompletion: true  }    }";
        var aceHtmlOption = "{mode: 'html', require: ['ace/ext/language_tools'],advanced: {enableSnippets: true, enableBasicAutocompletion: true, enableLiveAutocompletion: true  }    }";
        var aceJavaTemplate = '<div id="wrap"><div style="height: 300px;" ui-ace="' + aceJavaOption + '" ng-model="value"/></div>';
        var aceHtmlTemplate = '<div id="wrap"><div style="height: 300px;" ui-ace="' + aceHtmlOption + '" ng-model="value"/></div>';
        var script = nga.entity('script');
        script.listView().fields([
            nga.field('id'),
            nga.field('descripcion')
        ]).listActions(['show']);
        script.showView().fields([
            nga.field('id'),
            nga.field('descripcion'),
            nga.field('script').template(aceJavaTemplate)
        ]);
        script.creationView()
                .fields([
                    nga.field('descripcion'),
                    nga.field('script').template(aceJavaTemplate),
                    nga.field('status'),
                    nga.field('custom_action')
                            .label('')
                            .template('<test-script entry="entry"></test-script>')
                ]);
        script.editionView().fields(script.creationView().fields());

        /* static files */
        var staticFile = nga.entity('file');
        staticFile.listView().fields([
            nga.field('id'),
            nga.field('fileName'),
            nga.field('path')
        ]);

        var application = nga.entity('application');
application.listView().fields([
            nga.field('id'),
            nga.field('name'),
            nga.field('description')
        ]).listActions(['show']);
        
        staticFile.creationView().fields([
            nga.field('fileName'),
            nga.field('path'),
            nga.field('content').template(aceHtmlTemplate),
              nga.field('applicationId', 'reference').label('Application')
        .targetEntity(application) // Select a target Entity
        .targetField(nga.field('name')) // Select the field to be displayed
        ]);

        staticFile.editionView().fields(staticFile.creationView().fields());

        /* applications */
        
        application.creationView().fields([
            nga.field('name'),
            nga.field('description')
        ]);
        application.editionView().fields(application.creationView().fields());
        application.showView().fields([
            nga.field('name'),
            nga.field('description'),
            nga.field('files', 'referenced_list')
                    .targetEntity(staticFile)
                    .targetReferenceField('application_id')
                    .targetFields(staticFile.listView().fields())
                    .sortField('created_at')
                    .sortDir('DESC')
                    .listActions(['edit'])
        ]);

        admin.errorMessage(function (response) {
            return 'Error: ' + response.data.status + ' (' + response.data.mensaje + ')';
        });

        admin.addEntity(script);
        admin.addEntity(interface);
        admin.addEntity(server);
        admin.addEntity(staticFile);
        admin.addEntity(application);

        nga.configure(admin);
    }]);


