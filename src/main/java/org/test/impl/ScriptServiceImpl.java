package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import static ar.gov.santafe.meduc.serviceLocator.ServiceLocator.getService;
import groovy.lang.GroovyShell;
import java.util.List;
import org.test.interfaces.ScriptService;
import org.test.interfaces.ServerService;

/**
 *
 * @author enorrmann
 */
public class ScriptServiceImpl implements ScriptService {

    String fakelogicUrl = "http://localhost:3000/";
    String logicUrl = "http://localhost:8080/cfx-rest-services/api/";
    ScriptService fakeScriptService = getService(ScriptService.class, fakelogicUrl);
    ServerService ss = getService(ServerService.class,logicUrl);

    public List<SimpleDto> all() {
        return fakeScriptService.all();
    }

    public SimpleDto findById(String scriptId) {
        return fakeScriptService.findById(scriptId);
    }

    public SimpleDto create(SimpleDto script) {
        return fakeScriptService.create(script);
    }

    public SimpleDto update(String id, SimpleDto script) {
        //ss.restartServer("1");
        return fakeScriptService.update(id, script);
    }

    public SimpleDto delete(String scriptId) {
        return fakeScriptService.delete(scriptId);
    }

    public SimpleDto test(SimpleDto simpleDto) {
        GroovyShell shell = new GroovyShell();
        String mensaje = "Compila ok";

        try {
            Object res = shell.evaluate(simpleDto.getString("script"));

        } catch (Exception e){
            mensaje = e.getMessage();
        }
        SimpleDto gdto = new SimpleDto();
        gdto.add("mensaje", mensaje);
        return gdto;
    }

}
