package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import static ar.gov.santafe.meduc.serviceLocator.ServiceLocator.getService;
import java.util.List;
import org.test.interfaces.FileService;

/**
 *
 * @author enorrmann
 */
public class FileServiceImpl implements FileService{

    String fakelogicUrl = "http://localhost:3000/";
    FileService fs = getService(FileService.class, fakelogicUrl);

    @Override
    public List<SimpleDto> all() {
        return fs.all();
    }

    @Override
    public SimpleDto findById(String id) {
        return fs.findById(id);
    }

    @Override
    public SimpleDto create(SimpleDto simpleDto) {
        return fs.create(simpleDto);
    }

    @Override
    public SimpleDto update(String id, SimpleDto simpleDto) {
        return fs.update(id, simpleDto);
    }

    @Override
    public SimpleDto delete(String id) {
        return fs.delete(id);
    }

    @Override
    public String getContent(String id) {
        SimpleDto aFile = fs.findById(id);
        return aFile.getString("content");
    }
    
}
