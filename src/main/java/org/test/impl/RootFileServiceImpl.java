package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import static ar.gov.santafe.meduc.serviceLocator.ServiceLocator.getService;
import java.util.List;
import javax.ws.rs.Path;
import org.test.interfaces.FileService;

/**
 *
 * @author enorrmann
 */
@Path("/")
public class RootFileServiceImpl implements FileService {

    String fakelogicUrl = "http://localhost:3000/";
    FileService fs = getService(FileService.class, fakelogicUrl);

    @Override
    public String getContent(String fileName) {
        List<SimpleDto> allFiles = fs.all();
        for (SimpleDto aFile :allFiles){
            if (aFile.getString("fileName").equals(fileName)){
                return aFile.getString("content");
            }
        }
        return "404";
    }

    @Override
    public List<SimpleDto> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SimpleDto findById(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SimpleDto create(SimpleDto simpleDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SimpleDto update(String id, SimpleDto simpleDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SimpleDto delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
