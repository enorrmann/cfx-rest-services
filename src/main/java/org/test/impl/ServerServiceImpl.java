package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import ar.gov.santafe.meduc.serviceLocator.ServiceLocator;
import static ar.gov.santafe.meduc.serviceLocator.ServiceLocator.getService;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import org.test.configuration.server.CustomServer;
import org.test.interfaces.ScriptService;
import org.test.interfaces.ServerService;

@Stateless
public class ServerServiceImpl implements ServerService {

    String realDbUrl = "http://localhost:8080/cfx-rest-services/api/";
    String fakeDbUrl = "http://localhost:3000/";
    ServerService fakeDbServerService = getService(ServerService.class, fakeDbUrl);
    List<SimpleDto> runningServers = new ArrayList<>();
    
    public SimpleDto startServer() {
        SimpleDto sdto = new SimpleDto();
        CustomServer.start(loadScripts());
        sdto.add("started", CustomServer.isStarted());
        return sdto;
    }

    public SimpleDto stopServer() {
        SimpleDto sdto = new SimpleDto();
        CustomServer.stop();
        sdto.add("started", CustomServer.isStarted());
        return sdto;
    }

    private List<Object> loadScripts() {
        ScriptService ls = getService(ScriptService.class, realDbUrl);
        List<SimpleDto> scripts = ls.all();

        Binding binding = new Binding();
        binding.setProperty("serviceLocator", new ServiceLocator());

        GroovyShell shell = new GroovyShell(binding);

        List loadedScripts = new ArrayList();
        for (SimpleDto unScript : scripts) {
            Object logic = shell.evaluate(unScript.getString("script"));
            loadedScripts.add(logic);
        }
        return loadedScripts;

    }

    @Override
    public SimpleDto startServer(String id) {
        SimpleDto aServer = fakeDbServerService.findById(id);
        if (!runningServers.contains(aServer)){
            runningServers.add(aServer);
        }
        return startServer().add("status", "OK");
    }

    @Override
    public SimpleDto stopServer(String id) {
        SimpleDto aServer = fakeDbServerService.findById(id);
        if (runningServers.contains(aServer)){
            runningServers.remove(aServer);
        }
        return stopServer().add("status", "OK");
    }

    @Override
    public List<SimpleDto> all() {
        List<SimpleDto> servers = fakeDbServerService.all();
        for (SimpleDto unServer : servers) {
            if (runningServers.contains(unServer)){
                unServer.add("status", "Running");
            } else {
                unServer.add("status", "Detenido");
            }
        }
        return servers;
    }

    @Override
    public SimpleDto findById(String id) {
        return fakeDbServerService.findById(id);
    }

    @Override
    public SimpleDto create(SimpleDto simpleDto) {
        return fakeDbServerService.create(simpleDto);
    }

    @Override
    public SimpleDto update(String id, SimpleDto simpleDto) {
        return fakeDbServerService.update(id, simpleDto);
    }

    @Override
    public SimpleDto delete(String id) {
        return fakeDbServerService.delete(id);
    }

    @Override
    public SimpleDto restartServer(String id) {
        stopServer(id);
        return startServer(id);
    }

}
