package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import static ar.gov.santafe.meduc.serviceLocator.ServiceLocator.getService;
import java.util.ArrayList;
import java.util.List;
import org.test.interfaces.ApplicationService;
import org.test.interfaces.FileService;
import org.test.interfaces.ScriptService;

/**
 *
 * @author enorrmann
 */
public class ApplicationServiceImpl implements ApplicationService {

    String fakelogicUrl = "http://localhost:3000/";
    ApplicationService gs = getService(ApplicationService.class, fakelogicUrl);
    FileService fileService = getService(FileService.class, fakelogicUrl);
    ScriptService scriptService = getService(ScriptService.class, fakelogicUrl);

    @Override
    public List<SimpleDto> all() {
        return gs.all();
    }

    @Override
    public SimpleDto findById(String id) {
        return gs.findById(id);
    }

    @Override
    public SimpleDto create(SimpleDto simpleDto) {
        return gs.create(simpleDto);
    }

    @Override
    public SimpleDto update(String id, SimpleDto simpleDto) {
        return gs.update(id, simpleDto);
    }

    @Override
    public SimpleDto delete(String id) {
        return gs.delete(id);
    }

    @Override
    public List<SimpleDto> files(String id) {
        List<SimpleDto> allFiles = fileService.all();
        ArrayList<SimpleDto> myFiles = new ArrayList<SimpleDto>();
        for (SimpleDto aFile : allFiles) {
            String appId = aFile.getString("applicationId");
            if (appId != null && appId.equals(id)) {
                myFiles.add(aFile);
            }
        }
        return myFiles;
    }

    @Override
    public List<SimpleDto> scripts(String id) {
        List<SimpleDto> scripts = scriptService.all();
        ArrayList<SimpleDto> myScripts = new ArrayList<SimpleDto>();
        for (SimpleDto aScript : scripts) {
            String appId = aScript.getString("applicationId");
            if (appId != null && appId.equals(id)) {
                myScripts.add(aScript);
            }
        }
        return myScripts;
    }

}
