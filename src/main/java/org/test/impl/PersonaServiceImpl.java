package org.test.impl;

import org.test.model.Persona;
import ar.gov.santafe.meduc.dto.SimpleDto;
import ar.gov.santafe.meduc.interfaces.PersonaService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class PersonaServiceImpl implements PersonaService {

    @PersistenceContext(unitName = "SigaeEJBPU")
    private EntityManager entityManager;

    private static int ID_SEQ = 1;

    public SimpleDto getPersona(String nombre) {
        Query query = entityManager.createNativeQuery("select ID_PERSONA,APELLIDO_SOLTERO,NOMBRES,FECHA_NACIMIENTO from lp_persona where rownum < 2");

        List list = query.getResultList();
        SimpleDto pp = getMockPersona(nombre);
        List amigos = new ArrayList();
        SimpleDto paco = getMockPersona("paco");
        SimpleDto juan = getMockPersona("juan");
        amigos.add(paco);
        amigos.add(juan);
        pp.add("mejorAmigo", paco);
        pp.add("amigos", amigos);
        SimpleDto ciudad = new SimpleDto();
        ciudad.add("codigoPostal", 2342);
        //  pp.add("localidad", ciudad);
        pp.add("fechaNacimiento", new Date());
        return pp;
    }

    public SimpleDto getMockPersona(String nombre) {

        SimpleDto pp = new SimpleDto();
        pp.add("nombre", nombre);
        pp.add("id", ID_SEQ++);
        return pp;
    }

    @Override
    public SimpleDto create(SimpleDto personaDto) {
        Persona persona = personaDto.as(Persona.class);
        return new SimpleDto(persona);
    }

    @Override
    public SimpleDto findById(String id) {
        SimpleDto persona = getMockPersona("Pepe");
        persona.add("id", id);
        return persona;
    }

    @Override
    public List all() {
        List<SimpleDto> personas = new ArrayList<SimpleDto>();
        personas.add(getMockPersona("Pepe"));
        personas.add(getMockPersona("Mario"));
        personas.add(getMockPersona("Juan"));
        SimpleDto personasDto = new SimpleDto();
        personasDto.add("personas", personas);
        return personas;
        

    }

    public SimpleDto update(String id, SimpleDto simpleDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public SimpleDto delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
