package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ws.rs.Path;
import org.reflections.Reflections;
import org.test.interfaces.InterfaceService;

/**
 *
 * @author enorrmann
 */
public class InterfaceServiceImpl implements InterfaceService {

    public List<SimpleDto> all() {
        List interfaces = new ArrayList<SimpleDto>();

        Reflections reflections = new Reflections("ar.gov.santafe.meduc.interfaces");

        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Path.class);
        for (Class aClass : annotated) {
            Path path = (Path) aClass.getAnnotation(Path.class);
            
            SimpleDto interfaceDto = new SimpleDto()
                    .add("id", aClass.toString())
                    .add("interface", aClass.getName())
                    .add("path", path.value());
            interfaces.add(interfaceDto);
        }
        return interfaces;
    }

    public SimpleDto findById(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public SimpleDto create(SimpleDto simpleDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public SimpleDto update(String id, SimpleDto simpleDto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public SimpleDto delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
