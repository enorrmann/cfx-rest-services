package org.test.impl;

import ar.gov.santafe.meduc.dto.SimpleDto;
import ar.gov.santafe.meduc.serviceLocator.ServiceLocator;
import static ar.gov.santafe.meduc.serviceLocator.ServiceLocator.getService;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import javax.ejb.Stateless;
import org.test.interfaces.GroovyService;
import org.test.interfaces.ScriptService;


@Stateless
public class GroovyServiceImpl implements GroovyService {

    @Override
    public SimpleDto doSomething() {
        String logicUrl = "http://localhost:3000/";
        ScriptService ls = getService(ScriptService.class, logicUrl);
/*        String script = "import org.test.interfaces.*; PersonaService ps = serviceLocator.getService(PersonaService.class);ps.getPersona(\"pepe\")";
        SimpleDto scriptDto = new SimpleDto();
        scriptDto.add("id", "test_script");
        scriptDto.add("script", script);
        ls.createScript(scriptDto);*/
        SimpleDto loadedScript = ls.findById("test_script");

        Binding binding = new Binding();
        binding.setProperty("serviceLocator", new ServiceLocator());

        GroovyShell shell = new GroovyShell(binding);

        Object res = shell.evaluate(loadedScript.getString("script"));
        SimpleDto gdto = new SimpleDto();
        gdto.add("res", res);
        return gdto;

    }

}
