package org.test.configuration;

import ar.gov.santafe.meduc.serviceLocator.CustomJsonProvider;
import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();

        resources.add(CustomJsonProvider.class);
        addRestResourceClasses(resources);
        resources.add(org.test.impl.PersonaServiceImpl.class);
        resources.add(org.test.impl.FileServiceImpl.class);
        resources.add(org.test.impl.ServerServiceImpl.class);
        resources.add(org.test.impl.GroovyServiceImpl.class);
        resources.add(org.test.impl.ScriptServiceImpl.class);
        resources.add(org.test.impl.InterfaceServiceImpl.class);
        resources.add(org.test.impl.ApplicationServiceImpl.class);
        return resources;
    }
    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(org.test.configuration.CORSResponseFilter.class);
        resources.add(org.test.impl.RootFileServiceImpl.class);
    }



}
