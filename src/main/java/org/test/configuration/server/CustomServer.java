package org.test.configuration.server;

import ar.gov.santafe.meduc.serviceLocator.CustomJsonProvider;
import java.util.ArrayList;
import java.util.List;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;

/**
 *
 * @author enorrmann
 */
public class CustomServer {

    private static Server instance;

    private static void init(List services) {
        JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
        CustomJsonProvider jsonProvider = new CustomJsonProvider();
        List providers = new ArrayList();
        providers.add(jsonProvider);
        sf.setProviders(providers);
        List rc = new ArrayList();
        for (Object unServicio : services) {
            rc.add(unServicio.getClass());
            sf.setResourceProvider(unServicio.getClass(), new SingletonResourceProvider(unServicio));
        }
        sf.setResourceClasses(rc);
        sf.setAddress("http://localhost:9090");
        
        instance = sf.create();
    }

    public static void start(List services) {
        init(services);
        if (!instance.isStarted()) {
            instance.start();
        }
    }

    public static void stop() {
        if (instance.isStarted()) {
            instance.stop();
            instance.destroy();
        }
    }

    public static boolean isStarted() {
        return instance.isStarted();
    }
}
