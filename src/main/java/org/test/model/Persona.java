package org.test.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "LP_PERSONA")
public class Persona implements Serializable {

    @Id
    @GeneratedValue(generator = "SEC_LP_PERSONA")
    @SequenceGenerator(name = "SEC_LP_PERSONA", sequenceName = "SEC_LP_PERSONA", allocationSize = 1)
    @Column(name = "ID_PERSONA")
    private Long id;
    @Column(name = "SEXO")
    private String sexo;
    @Column(name = "APELLIDO_SOLTERO")
    private String apellidoSoltero;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(value = TemporalType.DATE)
    private Date fechaNacimiento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getApellidoSoltero() {
        return apellidoSoltero;
    }

    public void setApellidoSoltero(String apellidoSoltero) {
        this.apellidoSoltero = apellidoSoltero;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

}
