package org.test.interfaces;

import ar.gov.santafe.meduc.dto.SimpleDto;
import ar.gov.santafe.meduc.interfaces.GenericCrudService;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author enorrmann
 */
@Path("application")
public interface ApplicationService extends GenericCrudService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/files")
    public List<SimpleDto> files(@PathParam("id") String id);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/scripts")
    public List<SimpleDto> scripts(@PathParam("id") String id);

}
