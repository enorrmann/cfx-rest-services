package org.test.interfaces;

import ar.gov.santafe.meduc.dto.SimpleDto;
import ar.gov.santafe.meduc.interfaces.GenericCrudService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("server")
public interface ServerService extends GenericCrudService {

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/start")
    public SimpleDto startServer(@PathParam("id") String id);

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/stop")
    public SimpleDto stopServer(@PathParam("id") String id);

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}/restart")
    public SimpleDto restartServer(@PathParam("id") String id);

}
