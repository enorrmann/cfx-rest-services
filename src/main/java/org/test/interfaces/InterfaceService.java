package org.test.interfaces;

import ar.gov.santafe.meduc.interfaces.GenericCrudService;
import javax.ws.rs.Path;


@Path("interface")
public interface InterfaceService extends GenericCrudService{

}
