package org.test.interfaces;

import ar.gov.santafe.meduc.dto.SimpleDto;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("groovy")
public interface GroovyService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public SimpleDto doSomething();
}
