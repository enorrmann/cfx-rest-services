package org.test.interfaces;

import ar.gov.santafe.meduc.interfaces.GenericCrudService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author enorrmann
 */
@Path("file")
public interface FileService extends GenericCrudService {

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("{id}")
    public String getContent(@PathParam("id") String id);

}
